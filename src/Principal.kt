
fun main(){

    var num1:Double= 0.0
    var num2:Double=0.0
    var result:Double=0.0
    var option:Int=1


    while (option!=7) {

        println("Choose one of the following options:")
        println("1.Add the numbers")
        println("2.Substract the numbers")
        println("3.Multiply the numbers")
        println("4.Divide two numbers")
        println("5.Power of one number with another")
        println("6.Square root of one number")
        println("7.Exit")
        option = readLine()!!.toInt()



        if (option == 1) {

            println("Write number 1 and press enter")
            num1=readLine()!!.toDouble()

            println("Write number 2 and press enter")
            num2=readLine()!!.toDouble()

            result=sumar(num1,num2)

        } else if (option == 2) {

            println("Write number 1 and press enter")
            num1=readLine()!!.toDouble()

            println("Write number 2 and press enter")
            num2=readLine()!!.toDouble()

            result=restar(num1,num2)

        } else if (option == 3) {

            println("Write number 1 and press enter")
            num1=readLine()!!.toDouble()

            println("Write number 2 and press enter")
            num2=readLine()!!.toDouble()

            result=multiplicar(num1,num2)

        } else if (option == 4) {

            println("Write number 1 and press enter")
            num1=readLine()!!.toDouble()

            println("Write number 2 and press enter")
            num2=readLine()!!.toDouble()

            result=dividir(num1,num2)

        } else if (option == 5) {

            println("Write the base and press enter")
            num1=readLine()!!.toDouble()

            println("Write the exponent and press enter")
            num2=readLine()!!.toDouble()

            result=Math.pow(num1,num2)

        } else if (option == 6) {

            println("Write the number and press enter")
            num1=readLine()!!.toDouble()

            result=Math.sqrt(num1)
        }
        if (option >= 1 && option <= 6) {
            println("Your solution is: " + result)
        } else {
            println("Goodbye")
        }

    }

}


fun sumar(n1:Double,n2:Double):Double{
    var res:Double=n1+n2
    return res
}

fun restar(n1:Double,n2:Double):Double{
    var res:Double=n1-n2
    return res
}

fun multiplicar(n1:Double,n2:Double):Double{
    var res:Double=n1*n2
    return res
}

fun dividir(n1:Double,n2:Double):Double{
    var res:Double=n1/n2
    return res
}