
fun main(){

    var dani:Human=Human()
    dani.altura=1.72
    dani.edad=22
    dani.nombre="Daniel"

    var ale:Human=Human()
    ale.altura=1.72
    ale.edad=19
    ale.nombre="Alejandra"

    var generica:Human
    generica=dani

    println("Altura de Dani: "+dani.altura)

    println("Altura de "+generica.nombre+" es: "+generica.edad)
    generica.cumpleanos()
    println("Altura de "+generica.nombre+" es: "+generica.edad)

    println("Vida de Ale es "+ale.healthPoints)
    ale.golpeCaida()
    println("Vida de Ale es "+ale.healthPoints)


}